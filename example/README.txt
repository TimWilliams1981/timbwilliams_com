Hey Guys,

Each directory has a different .htaccess file inside it!
There are comments to help explain each what each of the
different directives are doing.  But, if you have any
questions just let me know!

You can always try the Apache References as well:
http://httpd.apache.org/docs/trunk/mod/core.html

Cheers,
Joseph Pecoraro
joepeck02@gmail.com
http://blog.bogojoker.com
